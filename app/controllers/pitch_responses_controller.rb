class PitchResponsesController < ApplicationController
  before_action :only_admins, only: %i[index]
  def index
    render json: PitchResponse.order(:created_at)
                              .includes(:pitch)
                              .as_json(include: :pitch)
  end

  def create
    resp = Pitch.find(params[:pitch_id])
                .pitch_responses
                .create(params.permit(:note, :email, :name, :like, :responder_type, :tag))

    identifier = [resp.name, resp.email, resp.tag.presence, resp.responder_type].join('/')
    Notification.create(text: "New pitch response! #{identifier} #{resp.like ? 'liked' : "didn't like"} "\
                              "'#{resp.pitch.text.truncate(20)}':\n>#{resp.note}")
    head :ok
  end
end
