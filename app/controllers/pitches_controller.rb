class PitchesController < ApplicationController
  before_action :only_admins, only: %i[approve destroy unapproved]
  def index
    render json: Pitch.where(approved: true).as_json(only: %i[id genre text])
  end

  def create
    Pitch.create(params.permit(:genre, :email, :text))

    Notification.create(text: 'New pitch!')

    head :ok
  end

  def approve
    Pitch.find(params[:id]).update(approved: true)
    head :ok
  end

  def destroy
    Pitch.find(params[:id]).destroy
    head :ok
  end

  def unapproved
    render json: Pitch.where(approved: [false, nil]).as_json(only: %i[id genre text email])
  end
end
