class ApplicationController < ActionController::Base
  skip_before_action :verify_authenticity_token

  def only_admins
    setting = Setting.find_by(key: 'admin_api_key')

    return if setting && setting.value == request.headers['X-API-KEY']

    head :unauthorized
  end
end
