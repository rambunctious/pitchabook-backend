class Notification
  class << self
    def create(text:)
      setting = Setting.find_by(key: 'slack_webhook')
      return unless setting

      notifier = Slack::Notifier.new setting.value,
                                     channel: '#general',
                                     username: 'Pitchabot'

      notifier.post(text: text, icon_emoji: ':tada:')

    end

  end
end
