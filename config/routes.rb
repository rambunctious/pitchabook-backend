Rails.application.routes.draw do
  resources :pitches, only: %i[index create destroy] do
    collection do
      get :unapproved
    end

    member do
      post :approve
    end
    resources :pitch_responses, only: :create
  end
  resources :pitch_responses, only: :index
end
