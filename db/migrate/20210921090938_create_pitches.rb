class CreatePitches < ActiveRecord::Migration[6.0]
  def change
    create_table :pitches do |t|
      t.string :genre
      t.text :text
      t.string :email
      t.boolean :approved
      t.timestamps
    end

    create_table :pitch_responses do |t|
      t.references :pitch
      t.text :note
      t.string :email
      t.string :name
      t.boolean :like
      t.string :responder_type
      t.timestamps
    end
  end
end
