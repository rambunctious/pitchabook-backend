class AddTagToPitchResponse < ActiveRecord::Migration[6.0]
  def change
    add_column :pitch_responses, :tag, :string
  end
end
