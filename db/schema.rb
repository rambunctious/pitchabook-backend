# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_09_26_175554) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "pitch_responses", force: :cascade do |t|
    t.bigint "pitch_id"
    t.text "note"
    t.string "email"
    t.string "name"
    t.boolean "like"
    t.string "responder_type"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "tag"
    t.index ["pitch_id"], name: "index_pitch_responses_on_pitch_id"
  end

  create_table "pitches", force: :cascade do |t|
    t.string "genre"
    t.text "text"
    t.string "email"
    t.boolean "approved"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "settings", force: :cascade do |t|
    t.string "key"
    t.string "value"
  end

end
